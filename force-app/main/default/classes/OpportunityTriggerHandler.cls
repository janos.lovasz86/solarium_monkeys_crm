/*
Ha a Stage mező WaitingForApprove státuszban van, akkor készítünk egy ApproveQuote nevű Task objektumot.
Ha a Stage mező Order státuszban van, akkor Az Opportunity objektumra felvett custom orderedDate__c mezőjébe 
beíródik az aktuális dátum.
Ha a Stage mező Paid státuszban van, akkor készítünk egy DeliverAsset Task objektumot,
és küldünk egy email-t a kiszállítandó árú részleteiről a delivery manager-nek.
*/
public class OpportunityTriggerHandler {


    public static void handleBeforeInsert(List<Opportunity> triggerNew){
        for(Opportunity o : triggerNew){
            if(o.StageName != 'Qualifying'){
                o.StageName.addError('Opportunities can be created only in Qualifying status.');
            }
        }
    }
    
    
    public static void handleBeforeUpdate(Map<Id, Opportunity> triggerOldMap, List<Opportunity> triggerNew){
        for(Opportunity o : triggerNew){
            /*
            if(!canUpdateOpp(o, triggerOldMap.get(o.Id))){
                o.StageName.addError('Opportunities cannot degrade from Proposal/PriceQuote.');
            }
            */
            
            if(cannotUpdateOpp(o, triggerOldMap.get(o.Id))){
                o.StageName.addError('Opportunities cannot degrade from Paid.');
            }else if(isChangedFromClosedWon(o, triggerOldMap.get(o.Id))){
                o.StageName.addError('The stage of an Opportunity can not be changed from Closed Won.');
            }else if(isChangedFromClosedLost(o, triggerOldMap.get(o.Id))){
                o.StageName.addError('The stage of an Opportunity can not be changed from Closed Lost.');
            }
            
        }
    }
    
    //Miután beszúrtunk egy Opportunity rekordot, kell-e valamit változtatnunk a más rekordon?
    public static void handleAfterInsert(List<Opportunity> triggerNew){        

    }
    
    //Miután frissitünk egy Opportunity rekordot, az adott StageName alapján készitünk Task-okat.
    public static void handleAfterUpdate(Map<Id, Opportunity> triggerOldMap, List<Opportunity> triggerNew){
        
        //Kikeressük az Opportunity-khez tartozó User-eket.
        Set<Id> userIds = new Set<Id>();
        Set<Id> oppIds = new Set<Id>();
        for(Opportunity o: triggerNew){
            oppIds.add(o.Id);
            if(o.OwnerId != null){
               userIds.add(o.OwnerId);               
            }
        }
        //Lekérdezzük az Opportunity-khez tartozó User-ek ManagerId-jait.
        Map<Id,User> userMap = new Map<Id,User>([SELECT Id, ManagerId FROM User WHERE Id IN :userIds]);
        
        //Lekérdezzük azokat a User-eket, akiknek a UserRole-ja 'Deployment Manager'.
        List<User> deploymentManagerIds = [SELECT Id, Name, Username, UserRole.Name FROM User WHERE UserRole.Name = 'Deployment Manager' LIMIT 1];
        
        //Kikeressük azokat a Quote-okat, amelyeknek státusza Aproved és közük van a frissitett Opportunity-khez, és a quoteMapByIds-ba tesszük.       
        Map<Id,Quote> approvedQuotesMapByIds = new Map<Id,Quote>([SELECT Id, Name, OpportunityId, Status FROM Quote WHERE (Status = 'Approved' OR Status = 'Accepted') AND OpportunityId IN :oppIds]);
        
        //Kikeressük azokat a Quote-okat, amelyeknek státusza In Review és közük van a frissitett Opportunity-khez, és a quoteMapByIds-ba tesszük.       
        Map<Id,Quote> inReviewQuoteMapByIds = new Map<Id,Quote>([SELECT Id, Name, Status, OpportunityId FROM Quote WHERE Status = 'In Review' AND OpportunityId IN :oppIds]);
        
        //Kikeressük azokat a QuoteLineItem-eket, amelyeknek közük van a frissitett Opportunity-hez tartozó Accepted státuszú Quote-okhoz és a quoteLineItemMapByIds-ba tesszük.
        Set<Id> quoteIds = new Set<Id>();
        for(Id quoteId : approvedQuotesMapByIds.keySet()){
            quoteIds.add(approvedQuotesMapByIds.get(quoteId).Id);
        }	                   
        Map<Id,QuoteLineItem> approvedQuoteLineItemMapByIds = new Map<Id,QuoteLineItem>([SELECT Id, Quantity, Product2Id, QuoteId FROM QuoteLineItem WHERE QuoteId IN :quoteIds]);

       for(Opportunity opp : triggerNew) {
           //ha egyáltalán változott az elöző állapothoz képest a StageNanme
           if(opp.StageName != triggerOldMap.get(opp.id).StageName){
               if(opp.StageName == 'WaitingForApprove'){
                   String inReviewQId = '';
                   for(Id inReviewQuoteId : inReviewQuoteMapByIds.keySet()){
                       if(inReviewQuoteMapByIds.get(inReviewQuoteId).OpportunityId == opp.Id){
                           System.debug('Az Opp-hoz tartozó In Review Quote Id-ja: '+inReviewQuoteMapByIds.get(inReviewQuoteId).Id);
                           inReviewQId = inReviewQuoteMapByIds.get(inReviewQuoteId).Id;
                       }
                   }
                   Task approveQuoteTask = new Task(OwnerId = userMap.get(opp.OwnerId).ManagerId,//User, aki approval-ja a Quote-ot. A User Manager-e.
                                                    Status = 'Not Started',
                                                   	Subject = 'Other',
                                                   	Priority = 'Normal',
                                                   	ActivityDate = System.today(),
                                                   	WhoId = opp.ContactId,//Contact
                                                   	WhatId = opp.AccountId,//Account
                                                    Quote_For_Task__c = inReviewQId, //Quote (In Review)
                                                   	Description = 'Please approve this QuoteLineItem!');
                   insert approveQuoteTask;                                          
                }else if(opp.StageName == 'Order'){
                    //opp.OrderDate__c = System.today();
                }else if(opp.StageName == 'Paid'){
                    //Annyi Asset rekordot készitünk, amennyi QuoteLineItem van a approvedQuoteLineItemMapByIds Map-ben, betesszük egy listába és beszúrjuk a DB-be.
                    List<Asset> assetsToDB = new List<Asset>();
                    for(Id qliId : approvedQuoteLineItemMapByIds.keySet()){
                        Asset deliverAsset = new Asset(OwnerId = opp.OwnerId,
                                                  Name = 'A(z) '+opp.AccountId+' asset-je.',
                                                  AccountId = opp.AccountId,
                                                  ContactId = opp.ContactId,
                                                  Product2Id = approvedQuoteLineItemMapByIds.get(qliId).Product2Id,
                                                  Quantity = approvedQuoteLineItemMapByIds.get(qliId).Quantity,
                                                  Status = 'WaitingForDelivery',
                                                  Description = 'Please deliver this Asset to Client!');
                        assetsToDB.add(deliverAsset);
                        //System.debug('QLI Id '+qliId);
                    }
                    insert assetsToDB;
                    //Kikeressük az adott Opportunity-hez kapcsolodó összes Approved státuszu Quote-ot.
                    String approvedQId = '';
                    for(Id approvedQuoteId : approvedQuotesMapByIds.keySet()){
                       if(approvedQuotesMapByIds.get(approvedQuoteId).OpportunityId == opp.Id){
                           System.debug('Az Opp-hoz tartozó Approved Quote Id-ja: '+approvedQuotesMapByIds.get(approvedQuoteId).Id);
                           approvedQId = approvedQuotesMapByIds.get(approvedQuoteId).Id;
                       }
                   }                   
                    //A QuoteLineItem-ekhez tartozó Task rekordokat készitünk, az összes lookup mezővel.
                    List<Task> assetsTaskListToDB = new List<Task>();
                    String approvedQuoteLineItemId = '';
                    for(Id qliId : approvedQuoteLineItemMapByIds.keySet()){
                        if(approvedQuoteLineItemMapByIds.get(qliId).QuoteId == approvedQId){//Itt az Opp adott Quote Id-jável kell összehasonlitani.
                            approvedQuoteLineItemId = approvedQuoteLineItemMapByIds.get(qliId).Id;
                            Task deliverAssetTask = new Task(OwnerId = deploymentManagerIds.get(0).Id,//User, aki kiszállitja az szoligépeket. A Deployment Manager.
                                                    Status = 'Not Started',
                                                   	Subject = 'Deliver the item to the customer!',
                                                   	Priority = 'Normal',
                                                   	ActivityDate = System.today(),
                                                   	WhoId = opp.ContactId,//Contact
                                                   	WhatId = opp.AccountId,//Account
                                                    Asset_For_Task__c = approvedQuoteLineItemId,//Asset
                                                   	Description = 'Please deliver this asset to client!');
                            assetsTaskListToDB.add(deliverAssetTask);
                        }
                    }                    
                    //insert assetsTaskListToDB;                
                    //És itt még meg kéne hivni egy osztályt ami levelet küld a Deployment Manager-nek a kiszállitandó árúról: Mit, kinek, hova, mikor.
                    //Ez megvan Worklow rule-ban.
                }
           	}            
        }		
    }
    
    
    public static void handleAfterDelete(List<Opportunity> triggerOld){

    }

    private static Boolean canUpdateOpp(Opportunity newOpp, Opportunity oldOpp){
      return (isStageQualifyingToWaitingForApprove(newOpp, oldOpp)
              || isStageWaitingForApproveToQualifying(newOpp, oldOpp) 
              || isStageQualifyingToProposalPriceQuote(newOpp, oldOpp) 
              || isStageProposalPriceQuoteToQualifying (newOpp, oldOpp));
            
       
    }
    private static Boolean cannotUpdateOpp(Opportunity newOpp, Opportunity oldOpp){
        return (isStagePaidToOrder (newOpp, oldOpp)
            || isStageOrderToWaitingForApprove (newOpp, oldOpp)
            || isStageOrderToProposalPriceQuote (newOpp, oldOpp));
    }
    
    private static Boolean isStageQualifyingToWaitingForApprove(Opportunity newOpp, Opportunity oldOpp){
        return  oldOpp.StageName == 'Qualifying' && newOpp.StageName == 'WaitingForApprove';
    }
    
    private static Boolean isStageWaitingForApproveToQualifying(Opportunity newOpp, Opportunity oldOpp){
        return  oldOpp.StageName == 'WaitingForApprove' && newOpp.StageName == 'Qualifying';
    }
    
    private static Boolean isStageQualifyingToProposalPriceQuote(Opportunity newOpp, Opportunity oldOpp){
        return oldOpp.StageName == 'Qualifying' && newOpp.StageName == 'Proposal/Price Quote';
    }
    
    private static Boolean isStageProposalPriceQuoteToQualifying(Opportunity newOpp, Opportunity oldOpp){
        return oldOpp.StageName == 'Proposal/Price Quote' && newOpp.StageName == 'Qualifying';
    }
    
    private static Boolean isStagePaidToOrder(Opportunity newOpp, Opportunity oldOpp){
        Boolean canDegradeOppFromPaidToOrder = false;
        if(oldOpp.StageName == 'Paid' && newOpp.StageName == 'Order'){
            canDegradeOppFromPaidToOrder = true;
        }
        return canDegradeOppFromPaidToOrder;
    }
    
    private static Boolean isStageOrderToWaitingForApprove(Opportunity newOpp, Opportunity oldOpp){
        Boolean canDegradeOppFromOrderToWaitingForApprove = false;
        if(oldOpp.StageName == 'Order' && newOpp.StageName == 'WaitingForApprove'){
            canDegradeOppFromOrderToWaitingForApprove = true;
        }
        return canDegradeOppFromOrderToWaitingForApprove;
    }
    
    private static Boolean isStageOrderToProposalPriceQuote(Opportunity newOpp, Opportunity oldOpp){
        Boolean canDegradeOppFromOrderToProposalPriceQuote = false;
        if(oldOpp.StageName == 'Order' && newOpp.StageName == 'Proposal/Price Quote'){
            canDegradeOppFromOrderToProposalPriceQuote = true;
        }
        return canDegradeOppFromOrderToProposalPriceQuote;
    }
    
    private static Boolean isChangedFromClosedWon(Opportunity newOpp, Opportunity oldOpp){
        Boolean canDegradeOppFromClosedWonToAny = false;
        if(oldOpp.StageName != newOpp.StageName && oldOpp.StageName == 'Closed Won'){
            canDegradeOppFromClosedWonToAny = true;
        }
        return canDegradeOppFromClosedWonToAny;
    }
    
    private static Boolean isChangedFromClosedLost(Opportunity newOpp, Opportunity oldOpp){
        Boolean canDegradeOppFromClosedLostToAny = false;
        if(oldOpp.StageName != newOpp.StageName && oldOpp.StageName == 'Closed Lost'){
            canDegradeOppFromClosedLostToAny = true;
        }
        return canDegradeOppFromClosedLostToAny;
    }
}

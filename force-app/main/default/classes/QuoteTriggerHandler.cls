/*
Quote frissités után:
- Kikeressük azokat a Opportunity-kat, amelyeknek közük van a beszúrt Quote-okhoz, azaz változott a QuoteLineItem-ük mennyisége..
- Megvizsgáljuk mi történik akkor a Quote státuszával, ha a QuoteLineItem-ek változtak. (A QuoteLineItem-ek változtatni tudják a Quote státuszát).
- És a Quote státuszától függően változtatjuk az Opportunity Stagename-jét:
	Ha a régi Quote státusza In Review volt és az új státusza Approved lett, akkor az Opportunity StageName-jét Proposal/Price Quote-ra állitjuk.
	Ha a régi Quote státusza Approved volt és az új státusza In Review lett, akkor az Opportunity StageName-jét WaitingForApprove-ra állitjuk.

Quote törlés után:
- Kikeressük azokat a Opportunity-kat, amelyeknek közük van a törölt Quote-okhoz.
- Megvizsgáljuk, hogy az Opportunity-kon vannak-e Quote-ok, ha egy Opportinity-n egyetlen Quote sincs, és az Opportunity Description mezője ki van töltve,
  akkor az Opportunity StageName-jét ClosedLost-ra állitjuk.
*/
public class QuoteTriggerHandler {
    
    public static final String QUALIFYING = 'Qualifying';
    public static final String WAITINGFORAPPROVE = 'WaitingForApprove';
    public static final String PROPOSALPRICEQUOTE = 'Proposal/Price Quote';
    public static final String CLOSEDLOST = 'Closed Lost';
    public static final String INREVIEW = 'In Review';
    public static final String APPROVED = 'Approved';

	
    public static void handleBeforeInsert(List<Quote> triggerNew){
	
    }
    
    
    public static void handleBeforeUpdate(Map<Id, Quote> triggerOldMap, List<Quote> triggerNew){
		
    }
    
    
    public static void handleAfterInsert(List<Quote> triggerNew){

        
    }
    
    
    public static void handleAfterUpdate(Map<Id, Quote> triggerOldMap, List<Quote> triggerNew){
        
        //Kikeressük azokat a Opportunity-kat, amelyeknek közük van a beszúrt Quote-okhoz, változott a QuoteLineItem-ük mennyisége, és a oppMapByIds-ba tesszük.
        Set<Id> oppIds = new Set<Id>();
        for(Quote q : triggerNew){
            if(q.OpportunityId != null && (triggerOldMap.get(q.Id).QuoteLineItems.size() != q.QuoteLineItems.size() || triggerOldMap.get(q.Id).Status != q.Status)){
                oppIds.add(q.OpportunityId);
            }            
        }        
        Map<Id,Opportunity> oppMapByIds = new Map<Id,Opportunity>([SELECT Id, StageName, OwnerId, (SELECT Id, Name FROM Quotes) FROM Opportunity WHERE Id IN :oppIds]);
        
        //Megvizsgáljuk mi történik akkor a Quote státuszával, ha a QuoteLineItem-ek változtak. (A QuoteLineItem-ek változtatni tudják a Quote státuszát),
        //akár több lett belőlük vagy kevesebb, akár a státusza változott.
        //És a Quote státuszától függően változtatjuk az Opportunity Stagename-jét.
		for(Quote q : triggerNew){
            if(q.OpportunityId != null){
                If((triggerOldMap.get(q.Id).QuoteLineItems.size() != q.QuoteLineItems.size()) || (triggerOldMap.get(q.Id).Status != q.Status)){
                    if(triggerOldMap.get(q.Id).Status == INREVIEW && q.Status == APPROVED){
                        System.debug('Stage Name: '+PROPOSALPRICEQUOTE);
                        oppMapByIds.get(q.OpportunityId).StageName = PROPOSALPRICEQUOTE;
                    }else if(triggerOldMap.get(q.Id).Status == APPROVED && q.Status == INREVIEW){
                        System.debug('Stage Name: '+WAITINGFORAPPROVE);
                        System.debug('**User id for opportunity owner**'+oppMapByIds.get(q.OpportunityId).OwnerId);
                        oppMapByIds.get(q.OpportunityId).StageName = WAITINGFORAPPROVE;
                    }
                }
            }    
        }
		update oppMapByIds.values();
    }
    
    
    public static void handleAfterDelete(List<Quote> triggerOld){
        
        //Kikeressük azokat a Opportunity-kat, amelyeknek közük van a törölt Quote-okhoz és a oppMapByIds-ba tesszük.
        Set<Id> oppIds = new Set<Id>();
        for(Quote q : triggerOld){
            if(q.OpportunityId != null){
                oppIds.add(q.OpportunityId);
            }            
        }        
        Map<Id,Opportunity> oppMapByIds = new Map<Id,Opportunity>([SELECT Id, StageName, Description, (SELECT Id, Name FROM Quotes) FROM Opportunity WHERE Id IN :oppIds]);
        
        //Megvizsgáljuk, hogy az Opportunity-kon vannak-e Quote-ok, ha egy Opportinity-n egyetlen Quote sincs, és az Opportunity Description mezője ki van töltve,
        //az Opportunity StageName-jét ClosedLost-ra állitjuk.
        for(Id oppId : oppMapByIds.keySet()){
            if(oppMapByIds.get(oppId).Quotes.size() == 0 && oppMapByIds.get(oppId).Description != null){
                oppMapByIds.get(oppId).StageName = CLOSEDLOST;
            }
        }        
		update oppMapByIds.values();
    }

}

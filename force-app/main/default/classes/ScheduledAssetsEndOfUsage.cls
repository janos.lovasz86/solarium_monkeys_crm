global class ScheduledAssetsEndOfUsage  implements Schedulable{
	global void execute(SchedulableContext ex){	
    	List<User> deploymentManagerIds = [
        							SELECT Id, Name, Username, UserRole.Name 
        							FROM User 
        							WHERE UserRole.Name = 'Deployment Manager' 
        							LIMIT 1
    								];
    	String deliverId = deploymentManagerIds.get(0).Id;
        List<Asset> assetsEnded = [
            					SELECT Id, Name, UsageEndDate, AccountId 
            					FROM Asset 
            					WHERE UsageEndDate = :System.today()
        						];
        if(assetsEnded.size() > 0){   
            for(Asset a: assetsEnded) {
                    Task getBackAsset = new Task();
                    getBackAsset.OwnerId = deliverId;
                    getBackAsset.Subject = 'Deliver the Asset from the Customer to the factory';
                    getBackAsset.Status = 'Open';
                    getBackAsset.Priority = 'Normal';
                    getBackAsset.WhatId = a.AccountId;
                	getBackAsset.Asset_For_Task__c = a.Id;
                    insert getBackAsset;
        	}
        }
    }
}
